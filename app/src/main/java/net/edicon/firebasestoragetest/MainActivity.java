package net.edicon.firebasestoragetest;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String BucketUrl = "gs://smart-vod.appspot.com";
        String childPath = "preposition" + "/"  + "preposition01.mp4";
        StorageReference storageRef = initFirebaseStorage( BucketUrl );
        StorageReference prepoFileRef = getChildReference( storageRef, childPath );
        addMetadataListener( prepoFileRef, metaSussessListener, metaFailureListener);
    }

    public static StorageReference initFirebaseStorage( String bucketUrl ) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl(bucketUrl);
        return storageRef;
    }

    public static StorageReference getChildReference( StorageReference rootRef, String childPath ) {
        return rootRef.child(childPath);
    }

    public static void addMetadataListener(StorageReference storageRef,
           OnSuccessListener<StorageMetadata> successListener,
           // OnCompleteListener<StorageMetadata> completeListener,
           OnFailureListener failureListener) {
       storageRef.getMetadata()
           .addOnSuccessListener(successListener)
           // .addOnCompleteListener(completeListener)
           .addOnFailureListener(failureListener);
    }

    private OnSuccessListener<StorageMetadata> metaSussessListener = new OnSuccessListener<StorageMetadata>() {
        @Override
        public void onSuccess(StorageMetadata storageMetadata) {
            Uri downloadUrl = storageMetadata.getDownloadUrl();
            List<Uri> downloadUrls = storageMetadata.getDownloadUrls();
            if( BuildConfig.DEBUG ) {
                Log.d("FIRESTORAGE", downloadUrl.toString());
                Log.d("FIRESTORAGE", downloadUrls.toString());
            }
        }
    };

    private OnCompleteListener<StorageMetadata> metaCompleteListener = new OnCompleteListener<StorageMetadata>() {
        @Override
        public void onComplete(@NonNull Task<StorageMetadata> task) {
            task.isComplete();
        }
    };

    private OnFailureListener metaFailureListener = new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
            e.printStackTrace();
        }
    };
}
